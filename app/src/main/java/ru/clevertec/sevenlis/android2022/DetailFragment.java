package ru.clevertec.sevenlis.android2022;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.transition.Transition;
import androidx.transition.TransitionInflater;

public class DetailFragment extends Fragment {
    private Context mContext;

    public DetailFragment() {
        super(R.layout.detail_fragment_layout);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Transition enterTransition = TransitionInflater.from(mContext).inflateTransition(android.R.transition.move);
            setSharedElementEnterTransition(enterTransition);
            Transition returnTransition = TransitionInflater.from(mContext).inflateTransition(android.R.transition.move);
            setSharedElementReturnTransition(returnTransition);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onResume() {
        super.onResume();

        ActionBar actionBar = ((AppCompatActivity) mContext).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.show();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ImageView detailImage = view.findViewById(R.id.imageView);
        ViewCompat.setTransitionName(detailImage, requireArguments().getString("tn_imageView"));

        TextView tvTitle = view.findViewById(R.id.title);
        tvTitle.setText(requireArguments().getString("title"));
        ViewCompat.setTransitionName(tvTitle, requireArguments().getString("tn_tvTitle"));

        TextView tvDescription = view.findViewById(R.id.description);
        tvDescription.setText(requireArguments().getString("description"));
        ViewCompat.setTransitionName(tvDescription, requireArguments().getString("tn_tvDescription"));

        Button buttonExit = view.findViewById(R.id.buttonExit);
        buttonExit.setOnClickListener(buttonView -> System.exit(0));
    }
}
